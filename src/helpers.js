export function totesRando(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

export function getRandomPlaceholder() {
  const placeholders = ['Hellboy', 'Superman', 'X-Men', 'Batman', 'Avengers'];

  return totesRando(placeholders);
}
