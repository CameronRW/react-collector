module.exports = {
  item1: {
    name: 'A Thing - Vol 1',
    image: 'https://source.unsplash.com/random/100x120',
    desc: 'A thing happened',
    owned: false
  },
  item2: {
    name: 'A Thing - Vol 2',
    image: 'https://source.unsplash.com/random/100x120',
    desc: 'A thing happened again',
    owned: false
  },
  item3: {
    name: 'A Thing - Vol 3',
    image: 'https://source.unsplash.com/random/100x120',
    desc: 'A thing keeps happening',
    owned: false
  },
}
