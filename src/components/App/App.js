import React, { Component } from 'react';
import Header from '../Header/Header';
import SearchBar from '../SearchBar/SearchBar';
import Collection from '../Collection/Collection';
import Footer from '../Footer/Footer';

import './App.css';

import sampleCollection from '../../sample-content';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Header title="Collector" />
        <p className="App-intro">
          Lorem ipsum.
        </p>
        <SearchBar />
        <Collection collectables={sampleCollection} />
        <Footer />
      </div>
    );
  }
}

export default App;
