import React, { Component } from 'react';
import './Footer.css';

class Footer extends Component {
  render() {
    return (
      <footer className="Footer">
        <p>&copy; Lorem ipsum.</p>
      </footer>
    );
  }
}

export default Footer;
