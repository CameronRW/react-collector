import React, { Component } from 'react';
import Collectable from '../Collectable/Collectable';
import Button from '../Button/Button';
import './Collection.css';

class Collection extends Component {
  constructor() {
    super();

    this.state = {
      collection: {}
    }

    this.loadSamples = this.loadSamples.bind(this);
  }

  loadSamples() {
    this.setState({
      collection: this.props.collectables
    });
  }

  render() {
    return (
      <div className="Collection">
        <ul>
        {
          Object.keys(this.state.collection).map(key => <Collectable key={key} index={key} details={this.state.collection[key]} />)
        }
        </ul>
        <Button value="Load Sample Content" onClick={this.loadSamples} />
      </div>
    );
  }
}

export default Collection;
