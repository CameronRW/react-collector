import React, { Component } from 'react';
import './Button.css';

class Button extends Component {
  constructor () {
    super();
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    this.props.onClick();
  }
  render() {
    return (
      <button className="Button" onClick={this.handleClick}>{this.props.value}</button>
    );
  }
}

export default Button;
