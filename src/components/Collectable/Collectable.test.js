import React from 'react';
import ReactDOM from 'react-dom';
import Collectable from './Collectable';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Collectable />, div);
  ReactDOM.unmountComponentAtNode(div);
});
