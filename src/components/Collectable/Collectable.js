import React, { Component } from 'react';
import './Collectable.css';

class Collectable extends Component {
  render() {
    const { details } = this.props;
    return (
      <li className="Collectable">
        <img src={details.image} alt={details.name} />
        <h3>{details.name}</h3>
        <p>{details.desc}</p>
      </li>
    );
  }
}

export default Collectable;
