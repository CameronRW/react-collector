import React, { Component } from 'react';
import Button from '../Button/Button';
import './SearchBar.css';

class Header extends Component {
  render() {
    return (
      <div className="SearchBar">
        <input className="SearchBar-input" type="text" />
        <Button value="Search" />
      </div>
    );
  }
}

export default Header;
